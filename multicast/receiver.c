#include "net.h"
//server
int main(int argc, char const *argv[])
{

	int sockfd = -1;

	if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1){
		perror("socket");
		exit(1);
	}
	int b_reuse = 1;

	// setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &b_reuse, sizeof(b_reuse));

	//填充服务端 结构体
	struct sockaddr_in sin;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(SERV_PORT);
	sin.sin_addr.s_addr = htons(INADDR_ANY);
	//加入多播组
	struct ip_mreq mreq;
	bzero(&mreq, sizeof(mreq));
	mreq.imr_multiaddr.s_addr = inet_addr(MULTICAST_IP);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	setsockopt(sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));

	//填充客户端 结构体
	struct sockaddr_in cin;
	cin.sin_family = AF_INET;
	if(bind(sockfd , (struct sockaddr*)&sin, sizeof(sin)) != 0){
		perror("bind");
		exit(1);
	}

	char buf[BUFSIZE];
	socklen_t sin_addrlen = sizeof(sin);
	socklen_t cin_addrlen = sizeof(cin);
	char client_ipv4[16];

	printf("Server is built up, multicast %s:%d!\n", MULTICAST_IP, SERV_PORT);
	while(1){
		bzero(buf, BUFSIZE);
		if(recvfrom(sockfd, buf, BUFSIZE-1, 0, (struct sockaddr*)&cin, &cin_addrlen) < 0){
			perror("recvfrom!");
			continue;
		}
		// inet_ntop(AF_INET, (void*)&cin.sin_addr, client_ipv4, cin_addrlen);
		printf("Receiving: %s", buf);

		sleep(1);
		// sendto(sockfd, buf, BUFSIZE-1, MSG_CMSG_CLOEXEC, (struct sockaddr*)&sin, sin_addrlen);
	}

	return 0;
}