#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <errno.h>
#include <math.h>


#define SERV_PORT  50002
#define SERV_IP_ADDR "81.70.145.175"
#define BACKLOG 5
#define BUFSIZE 64

#define FREQ 100
#define AMP 10
#define STEP 100


void sinuswave(int fd);
void send_command(int fd, char *cmd);
int main(int argc, char const *argv[])
{
	int fd = -1;
	struct sockaddr_in sin;
	// 1. 创建fd
	if( (fd = socket(AF_INET, SOCK_STREAM, 0) )== -1){
		perror("socket");
		exit(-1);
	}

	// 2. 绑定
	// 2.1 填充struct sockaddr_in
	bzero(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(SERV_PORT);
	if(inet_pton(AF_INET, SERV_IP_ADDR, (void *)&sin.sin_addr.s_addr) < 1){
		perror("inet_pton");
		exit(-1);
	}

	if ( connect(fd, (struct sockaddr *)&sin, sizeof(sin) ) == -1 ){
		perror("connect");
		exit(-1);
	}



	// 3. 读写数据
#if 0
	char buf[BUFSIZE];
	while(1){
		bzero(buf, BUFSIZE);

		if ( fgets(buf, BUFSIZE-1, stdin) == NULL){
			perror("fgets");
			exit(-1);
		}
		if ( !strncasecmp(buf, "quit", 4 )){
			printf("client is exiting!\n");
			break;
		}
		write(fd, buf, strlen(buf));
	}
#else 


		


#endif




	sinuswave(fd);


	return 0;
}
void sinuswave(int fd){
	float t = 0.0 , y = 0.0;
	char buf[BUFSIZE];
	char t_buf[20];
	char y_buf[20];

	for (int i = 0; i < STEP+1; i++){
		bzero(buf,sizeof(buf));
		bzero(t_buf,sizeof(t_buf));
		bzero(y_buf,sizeof(y_buf));

		t = ((float)i/STEP);

		y = AMP*sin(2*3.14*FREQ*t); //
		printf("i : %d t: %f, y: %f\n", i, t, y);

		sprintf(t_buf, "%.4f", t);
		sprintf(y_buf, "%.4f", y);


		strcpy(buf, t_buf);
		strcat(buf, ", " );
		strcat(buf, y_buf);
		strcat(buf, "\n");
		send_command(fd, buf);

		

	}


}
void send_command(int fd,char *cmd){

	write(fd, cmd, strlen(cmd));
	usleep(1000000/FREQ);


}





