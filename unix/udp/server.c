#include "net.h"

void *cli_data_handler(void *arg);
int main(int argc, char const *argv[])
{

	int sockfd = -1;

	sockfd = socket(AF_LOCAL, SOCK_DGRAM, 0);

	int b_reuse = 1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &b_reuse, sizeof(b_reuse));

	struct sockaddr_un myaddr;
	myaddr.sun_family = AF_LOCAL; 

	/*	如果sunpath所指向的文件存在，就删除。	*/
	if(access(CLIENT_SUNPATH, F_OK)== 0){
		unlink(CLIENT_SUNPATH);
	}

	strcpy(myaddr.sun_path, CLIENT_SUNPATH);


	if(bind(sockfd, (struct sockaddr *)&myaddr, sizeof(myaddr)) != 0){
		perror("bind");
		exit(-1);
	}


	printf("unix sever is up!\n");


	listen(sockfd, BACKLOG);
	socklen_t addr_len = sizeof(myaddr);
	pthread_t cli_thread;
	int acpt_fd;
	while(1){
		
		if(( acpt_fd = accept(sockfd, NULL, NULL)) < 0){
			perror("accept");
			exit(-1);
		}
		printf("unix client is connnected!\n");


		pthread_create(&cli_thread, NULL, cli_data_handler, (void*)&acpt_fd);
	}

	return 0;
}



void *cli_data_handler(void *arg){
	int newfd = *(int *)arg;

	int ret = -1;
	char buf[BUFSIZE];
	printf("Client Newfd: %d\n", newfd);

	while(1){
		bzero(&buf, BUFSIZE);
		do{
			if((ret = read(newfd, buf, BUFSIZE-1)) == -1){
				perror("read");
				exit(-1);
			}
		}while(ret > 0 && EINTR == errno);

		if ( ret < 0){
			perror("read");
			exit(-1);
		}
		if ( !ret ){ //接受到0个字符
			break;
		}
		printf("Received data: %s", buf);
	}
	printf("client has disconnected!\n");
	close(newfd);
	return 0;
}










