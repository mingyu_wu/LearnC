
/**
 * @Synopsis -fsanitize=address option is used to detect memory errors in C/C++ programs.
 *           使用时，需要在编译时加上 -fsanitize=address 选项
 *
 * @Param argc
 * @Param argv[]
 *
 * @return 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_SIZE 100

int main(int argc, char *argv[]) {

    char *str = (char *)malloc(STR_SIZE);

    if (str == NULL) {
        fprintf(stderr, "Memory allocation failed\n");
        return EXIT_FAILURE;
    }

    strcpy(str,
           "-fsanitize=address option is used to detect memory errors in C/C++ "
           "programs.");
    // Memory Leak
    printf("%s\n", str);

    free(str);
    // Another Memory Leak
    // printf("%s\n", str);
    return EXIT_SUCCESS;
}
